package com.parliament.app.exception;

import lombok.Data;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
class ResponseError {
    private String message;
    private String timeStamp;

    ResponseError(String message) {
        this.message = message;
        this.timeStamp = new SimpleDateFormat("yyyy.MM.dd - HH.mm.ss").format(new Date());
    }
}

@ControllerAdvice
public class Handler {

    @ExceptionHandler
    private ResponseEntity<ResponseError> incorrectSearchField(NoSuchFieldException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError("No such field: " + e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> invalidFormat(IllegalArgumentException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> thrownByController(ControllerException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError(e.getMessage()), e.getStatus());
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> duplicateEntry(ConstraintViolationException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError("Unique Constraint Violation"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> duplicateEntry(DataIntegrityViolationException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError("Data Integrity Violation"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> duplicateEntry(SQLIntegrityConstraintViolationException e) {
        e.printStackTrace();
        return new ResponseEntity<>(new ResponseError("SQLIntegrity Exception"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    private ResponseEntity<String> badRequest(Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
