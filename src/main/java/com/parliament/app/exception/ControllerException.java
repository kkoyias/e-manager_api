package com.parliament.app.exception;

import org.springframework.http.HttpStatus;

public class ControllerException extends RuntimeException {
    private final HttpStatus status;

    public ControllerException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
