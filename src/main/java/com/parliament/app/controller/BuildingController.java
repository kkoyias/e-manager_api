package com.parliament.app.controller;

import com.parliament.app.entity.building.Building;
import com.parliament.app.entity.building.service.Service;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.BuildingRepo;
import com.parliament.app.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;

@RepositoryRestController
@RequestMapping("/buildings")
public class BuildingController {

    private final ServiceRepo serviceRepo;
    private final BuildingRepo buildingRepo;

    @Autowired
    public BuildingController(ServiceRepo serviceRepo, BuildingRepo buildingRepo) {
        this.serviceRepo = serviceRepo;
        this.buildingRepo = buildingRepo;
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<Building>> getFullBuildingList() {
        return ResponseEntity.ok(this.buildingRepo.findAll());
    }

    @PostMapping("/{buildingID}/services")
    public @ResponseBody
    ResponseEntity<?> createService(
            @RequestBody Service service,
            @PathVariable Integer buildingID) {
        final Optional<Building> buildingOptional = this.buildingRepo.findById(buildingID);
        if (buildingOptional.isEmpty()) {
            throw new ControllerException("Building with id: " + buildingID + " does not exist", HttpStatus.NOT_FOUND);
        }

        final Building building = buildingOptional.get();
        service.setBuilding(building);
        building.addService(service);
        this.serviceRepo.save(service);
        return ResponseEntity.ok(service);
    }
}
