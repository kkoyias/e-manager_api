package com.parliament.app.controller;

import com.parliament.app.entity.item.action.info.ActionInfo;
import com.parliament.app.repo.ActionInfoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RepositoryRestController
@RequestMapping("/actionInfos")
public class ActionInfoController {

    private final ActionInfoRepo actionInfoRepo;

    @Autowired
    public ActionInfoController(ActionInfoRepo actionInfoRepo) {
        this.actionInfoRepo = actionInfoRepo;
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<ActionInfo>> getFullActionList() {
        return new ResponseEntity<>(this.actionInfoRepo.findAll(), HttpStatus.OK);
    }
}
