package com.parliament.app.controller.request;

import lombok.Data;

@Data
public class EmployeeDTO {

    private int id;
    private long code;
    private String firstName;
    private String lastName;
    private int serviceID;
}
