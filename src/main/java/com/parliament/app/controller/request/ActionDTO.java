package com.parliament.app.controller.request;

import lombok.Data;

import java.util.Date;

@Data
public class ActionDTO {

    private int id;
    private int actionInfoID;
    private Date actionDate;
    private int actorID;
    private int receiverID;
    private int itemID;

}
