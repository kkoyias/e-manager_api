package com.parliament.app.controller.request;

import lombok.Data;

@Data
public class ServiceDTO {

    private int id;
    private String name;
    private int buildingID;
    private int floor;
    private int managerID;
    private int parentServiceID;
}
