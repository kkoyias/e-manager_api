package com.parliament.app.controller.request;

import lombok.Data;

@Data
public class ItemDTO {
    private long barcode;
    private int typeID;
    private int ownerID;
}
