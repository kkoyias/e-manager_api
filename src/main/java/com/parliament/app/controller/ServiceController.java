package com.parliament.app.controller;

import com.parliament.app.controller.request.ServiceDTO;
import com.parliament.app.controller.response.ServiceProjection;
import com.parliament.app.entity.building.Building;
import com.parliament.app.entity.building.service.Service;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.BuildingRepo;
import com.parliament.app.repo.EmployeeRepo;
import com.parliament.app.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.stream.Stream;

@RepositoryRestController
@RequestMapping("/services")
public class ServiceController {

    private final ServiceRepo serviceRepo;
    private final BuildingRepo buildingRepo;
    private final EmployeeRepo employeeRepo;
    private final ProjectionFactory projectionFactory;

    @Autowired
    public ServiceController(ServiceRepo serviceRepo, BuildingRepo buildingRepo,
                             EmployeeRepo employeeRepo, ProjectionFactory projectionFactory) {
        this.serviceRepo = serviceRepo;
        this.buildingRepo = buildingRepo;
        this.employeeRepo = employeeRepo;
        this.projectionFactory = projectionFactory;
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<Stream<ServiceProjection>> getFullServiceList() {
        Stream<ServiceProjection> result = this.serviceRepo.findAll().stream()
                .map(s -> this.projectionFactory.createProjection(ServiceProjection.class, s));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private void saveOrUpdateService(ServiceDTO serviceDTO, Service service) {

        Building building = this.buildingRepo.findById(serviceDTO.getBuildingID()).orElse(null);
        service.setBuilding(building);
        service.setName(serviceDTO.getName());
        service.setFloor(serviceDTO.getFloor());
        service.setParentService(this.serviceRepo.findById(serviceDTO.getParentServiceID()).orElse(null));
        service.setManager(this.employeeRepo.findById(serviceDTO.getManagerID()).orElse(null));
        this.serviceRepo.save(service);
    }

    @PutMapping("/{serviceId}")
    public @ResponseBody
    ResponseEntity<?> updateService(
            @RequestBody ServiceDTO serviceDTO,
            @PathVariable Integer serviceId) {

        Service service = this.serviceRepo.findById(serviceId).orElse(null);
        if (service == null) {
            throw new ControllerException("Service with id " + serviceId + " does not exist", HttpStatus.NOT_FOUND);
        }

        this.saveOrUpdateService(serviceDTO, service);
        return new ResponseEntity<>(service, HttpStatus.OK);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<?> createService(
            @RequestBody ServiceDTO serviceDTO) {

        Service service = new Service();
        this.saveOrUpdateService(serviceDTO, service);
        return new ResponseEntity<>(service, HttpStatus.CREATED);
    }
}
