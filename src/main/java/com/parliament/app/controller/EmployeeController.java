package com.parliament.app.controller;

import com.parliament.app.controller.request.EmployeeDTO;
import com.parliament.app.entity.building.service.Service;
import com.parliament.app.entity.employee.Employee;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.EmployeeRepo;
import com.parliament.app.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Objects;

@RepositoryRestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeRepo employeeRepo;
    private final ServiceRepo serviceRepo;

    @Autowired
    public EmployeeController(EmployeeRepo employeeRepo, ServiceRepo serviceRepo) {
        this.employeeRepo = employeeRepo;
        this.serviceRepo = serviceRepo;
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<Employee>> getFullEmployeeList() {
        return new ResponseEntity<>(this.employeeRepo.findAll(), HttpStatus.OK);
    }

    private void saveOrUpdateEmployee(EmployeeDTO employeeDTO, Employee employee) {

        Service service = this.serviceRepo.findById(employeeDTO.getServiceID()).orElse(null);
        if (Objects.isNull(service)) {
            throw new ControllerException(
                    "Service with id " + employeeDTO.getServiceID() + " does not exist",
                    HttpStatus.NOT_FOUND);
        }

        employee.setService(service);
        employee.setCode(employeeDTO.getCode());
        employee.setFirstName(employeeDTO.getFirstName().replaceAll(" ", ""));
        employee.setLastName(employeeDTO.getLastName().replaceAll(" ", ""));
        this.employeeRepo.save(employee);

        service.addEmployee(employee);
        this.serviceRepo.save(service);
    }

    @PutMapping("/{employeeId}")
    public @ResponseBody
    ResponseEntity<?> updateService(
            @RequestBody EmployeeDTO employeeDTO,
            @PathVariable Integer employeeId) {

        Employee employee = this.employeeRepo.findById(employeeId).orElse(null);
        if (employee == null) {
            return new ResponseEntity<>("Employee with id " + employeeId + " does not exist", HttpStatus.NOT_FOUND);
        }

        this.saveOrUpdateEmployee(employeeDTO, employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<?> createService(@RequestBody EmployeeDTO employeeDTO) {

        Employee employee = new Employee();
        this.saveOrUpdateEmployee(employeeDTO, employee);
        return new ResponseEntity<>(employee, HttpStatus.CREATED);
    }
}
