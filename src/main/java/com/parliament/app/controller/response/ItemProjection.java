package com.parliament.app.controller.response;

import com.parliament.app.entity.item.Item;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "item-dto", types = {Item.class})
public interface ItemProjection {

    int getId();

    long getBarcode();

    @Value("#{target.owner != null ? target.owner.projectedName() : null }")
    String getOwner();

    @Value("#{target.type != null ? target.type.getName() : null}")
    String getType();
}
