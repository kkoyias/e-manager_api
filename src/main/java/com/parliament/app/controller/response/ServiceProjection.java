package com.parliament.app.controller.response;

import com.parliament.app.entity.building.service.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.config.Projection;

@RepositoryRestResource
@Projection(name = "service-dto", types = {Service.class})
public interface ServiceProjection {

    int getId();

    @Value("#{target.fullName()}")
    String getName();

    int getFloor();

    @Value("#{target.manager != null ? target.manager.projectedName() : null}")
    String getManager();

    @Value("#{target.parentService != null ? target.parentService.fullName() : null}")
    String getParentService();

    @Value("#{target.building != null ? target.building.getName() : null}")
    String getBuilding();
}
