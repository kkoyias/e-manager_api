package com.parliament.app.controller.response;

import com.parliament.app.entity.item.type.Type;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "type-dto", types = {Type.class})
public interface TypeProjection {
    int getId();

    String getName();
}
