package com.parliament.app.controller.response;

import com.parliament.app.entity.item.action.info.ActionInfo;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "actions-dto", types = {ActionInfo.class})
public interface ActionInfoProjection {
    int getId();

    String getName();
}
