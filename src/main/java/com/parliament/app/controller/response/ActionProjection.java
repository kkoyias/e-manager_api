package com.parliament.app.controller.response;

import com.parliament.app.entity.item.action.Action;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "history-dto", types = {Action.class})
public interface ActionProjection {

    int getId();

    @Value("#{target.actionInfo != null ? target.actionInfo.getName() : null}")
    String getActionInfo();

    Date getActionDate();

    @Value("#{target.actor != null ? target.actor.projectedName() : null}")
    String getActor();

    @Value("#{target.receiver != null ? target.receiver.projectedName() : null}")
    String getReceiver();

    ItemProjection getItem();
}
