package com.parliament.app.controller.response;

import com.parliament.app.entity.building.Building;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "building-dto", types = {Building.class})
public interface BuildingProjection {

    int getId();

    String getName();

    String getAddress();
}

