package com.parliament.app.controller.response;

import com.parliament.app.entity.employee.Employee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "employee-dto", types = {Employee.class})
public interface EmployeeProjection {

    int getId();

    long getCode();

    String getFirstName();

    String getLastName();

    @Value("#{target.service != null ? target.service.fullName() : null}")
    String getService();
}
