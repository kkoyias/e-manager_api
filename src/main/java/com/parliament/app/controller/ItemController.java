package com.parliament.app.controller;

import com.parliament.app.controller.request.ItemDTO;
import com.parliament.app.controller.response.ItemProjection;
import com.parliament.app.entity.employee.Employee;
import com.parliament.app.entity.item.Item;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.EmployeeRepo;
import com.parliament.app.repo.ItemRepo;
import com.parliament.app.repo.TypeRepo;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.stream.Stream;

@RepositoryRestController
public class ItemController {

    private final EmployeeRepo employeeRepo;
    private final ItemRepo itemRepo;
    private final TypeRepo typeRepo;
    private final ProjectionFactory projectionFactory;

    public ItemController(EmployeeRepo employeeRepo, ItemRepo itemRepo,
                          TypeRepo typeRepo, ProjectionFactory projectionFactory) {
        this.employeeRepo = employeeRepo;
        this.itemRepo = itemRepo;
        this.typeRepo = typeRepo;
        this.projectionFactory = projectionFactory;
    }

    @GetMapping("/items/all")
    public @ResponseBody
    ResponseEntity<Stream<ItemProjection>> getFullItemList() {
        Stream<ItemProjection> response = this.itemRepo.findAll().stream()
                .map(i -> this.projectionFactory.createProjection(ItemProjection.class, i));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private Item saveOrUpdateItem(ItemDTO itemDTO, Item item) {

        // set fields of same type
        item.setBarcode(itemDTO.getBarcode());

        // get & set owner or else null
        Employee employee = this.employeeRepo.findById(itemDTO.getOwnerID()).orElse(null);
        item.setOwner(employee);

        // set type
        item.setType(this.typeRepo.findById(itemDTO.getTypeID()).orElse(null));
        this.itemRepo.save(item);
        return item;
    }

    @PutMapping("/items/{itemID}")
    public @ResponseBody
    ResponseEntity<?> updateItem(
            @RequestBody ItemDTO itemDTO,
            @PathVariable Integer itemID) {

        // get the existing item
        Item item = this.itemRepo.findById(itemID).orElse(null);
        if (item == null) {
            throw new ControllerException("Item with id " + itemID + " does not exist", HttpStatus.NOT_FOUND);
        }

        // update it from the dto
        this.saveOrUpdateItem(itemDTO, item);

        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @PostMapping("/items")
    public @ResponseBody
    ResponseEntity<?> createItem(
            @RequestBody ItemDTO itemDTO) {

        // save a new item coming from the dto
        Item item = this.saveOrUpdateItem(itemDTO, new Item());
        return new ResponseEntity<>(item, HttpStatus.CREATED);
    }
}
