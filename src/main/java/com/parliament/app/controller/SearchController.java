package com.parliament.app.controller;

import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.EmployeeRepo;
import com.parliament.app.repo.ItemRepo;
import com.parliament.app.repo.Repo;
import com.parliament.app.repo.ServiceRepo;
import com.parliament.app.utils.Utils;
import com.parliament.app.utils.manager.ResponseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@BasePathAwareController
public class SearchController {

    private final Map<Class<?>, Repo> entityInfo;

    @Autowired
    public SearchController(ItemRepo itemRepo, EmployeeRepo employeeRepo, ServiceRepo serviceRepo) {

        this.entityInfo = new HashMap<Class<?>, Repo>() {{
            put(ItemRepo.class, itemRepo);
            put(EmployeeRepo.class, employeeRepo);
            put(ServiceRepo.class, serviceRepo);
        }};

    }

    @PostMapping("/{path}/search")
    public @ResponseBody
    ResponseEntity<?> filterEntity(
            @PathVariable String path,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestBody Map<String, Map<String, Object>> payload) throws NoSuchFieldException, ControllerException {

        // create a page request based on the parameters received
        PageRequest pageRequest = ResponseManager.getPageRequest(page, size, sort);

        // based on the path variable, find out which repo to use
        Class<?> repoClazz = Utils.findRepo(this.entityInfo.keySet(), path);
        if (repoClazz == null) {
            throw new ControllerException("Entity repository not found", HttpStatus.NOT_FOUND);
        }

        Repo repo = this.entityInfo.get(repoClazz);

        // respond with the page requested and some meta-data
        Map<?, ?> result = repo.find(payload, pageRequest,
                repoClazz.getAnnotation(RepositoryRestResource.class).excerptProjection());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
