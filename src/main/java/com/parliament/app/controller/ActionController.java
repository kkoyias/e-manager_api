package com.parliament.app.controller;

import com.parliament.app.controller.request.ActionDTO;
import com.parliament.app.controller.response.ActionProjection;
import com.parliament.app.entity.employee.Employee;
import com.parliament.app.entity.item.Item;
import com.parliament.app.entity.item.action.Action;
import com.parliament.app.entity.item.action.info.ActionInfo;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.ActionInfoRepo;
import com.parliament.app.repo.ActionRepo;
import com.parliament.app.repo.EmployeeRepo;
import com.parliament.app.repo.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RepositoryRestController
@RequestMapping("/actions")
public class ActionController {

    private final ActionRepo actionRepo;
    private final ActionInfoRepo actionInfoRepo;
    private final EmployeeRepo employeeRepo;
    private final ItemRepo itemRepo;
    private final ProjectionFactory projectionFactory;

    @Autowired
    public ActionController(ActionRepo actionRepo, ActionInfoRepo actionInfoRepo,
                            EmployeeRepo employeeRepo, ItemRepo itemRepo, ProjectionFactory projectionFactory) {
        this.actionRepo = actionRepo;
        this.actionInfoRepo = actionInfoRepo;
        this.employeeRepo = employeeRepo;
        this.itemRepo = itemRepo;
        this.projectionFactory = projectionFactory;
    }


    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<ActionProjection>> getFullActionList() {
        final List<ActionProjection> actionProjections = this.actionRepo.findAll().stream()
                .map(a -> this.projectionFactory.createProjection(ActionProjection.class, a))
                .collect(Collectors.toList());
        return ResponseEntity.ok(actionProjections);
    }

    private Action toAction(ActionDTO actionDTO) {
        int id = actionDTO.getId();
        Date date = actionDTO.getActionDate() != null ? actionDTO.getActionDate() : new Date();

        ActionInfo actionInfo = this.actionInfoRepo.findById(actionDTO.getActionInfoID()).orElse(null);
        Employee actor = this.employeeRepo.findById(actionDTO.getActorID()).orElse(null);
        Employee receiver = this.employeeRepo.findById(actionDTO.getReceiverID()).orElse(null);
        Item item = this.itemRepo.findById(actionDTO.getItemID()).orElse(null);

        return new Action(id, actionInfo, date, actor, receiver, item);
    }

    private Action saveOrUpdate(ActionDTO actionDTO) {
        Action action = this.toAction(actionDTO);

        // item field is mandatory
        if (action.getItem() == null) {
            throw new ControllerException("Each action must be associated with an item", HttpStatus.BAD_REQUEST);
        }

        // if request is valid, store action
        this.actionRepo.save(action);

        // set owner for the updated item, if this was a forward action later in time than the latest forward action
        Item item = action.getItem();
        if (this.actionRepo.isLatestAction(action.getActionDate(), item.getId()) && action.getReceiver() != null) {
            item.setOwner(action.getReceiver());
        }
        this.itemRepo.save(action.getItem());
        return action;
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<Action> createAction(@RequestBody ActionDTO actionDTO) {
        actionDTO.setId(0);
        return new ResponseEntity<>(this.saveOrUpdate(actionDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{actionID}")
    public @ResponseBody
    ResponseEntity<Action> updateAction(@RequestBody ActionDTO actionDTO,
                                        @PathVariable Integer actionID) {
        actionDTO.setId(actionID);
        return new ResponseEntity<>(this.saveOrUpdate(actionDTO), HttpStatus.OK);
    }

}
