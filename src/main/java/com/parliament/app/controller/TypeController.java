package com.parliament.app.controller;

import com.parliament.app.entity.item.type.Type;
import com.parliament.app.repo.TypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RepositoryRestController
@RequestMapping("/types")
public class TypeController {
    private final TypeRepo typeRepo;

    @Autowired
    public TypeController(TypeRepo typeRepo) {
        this.typeRepo = typeRepo;
    }

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<List<Type>> getFullTypeList() {
        return new ResponseEntity<>(this.typeRepo.findAll(), HttpStatus.OK);
    }
}
