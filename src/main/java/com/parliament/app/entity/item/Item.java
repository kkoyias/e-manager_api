package com.parliament.app.entity.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.parliament.app.entity.employee.Employee;
import com.parliament.app.entity.item.action.Action;
import com.parliament.app.entity.item.type.Type;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Data
@Entity
@Table(name = "item", uniqueConstraints = {@UniqueConstraint(columnNames = {"barcode"})})
public class Item implements Comparable<Item> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int id;

    @Column(name = "barcode")
    private long barcode;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "type_id")
    @Nullable
    private Type type;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "employee_id")
    private Employee owner;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Action> actions;

    @Override
    public int compareTo(Item item) {
        return (int) Math.floor(this.barcode - item.getBarcode());
    }

    @PreRemove
    private void setItemToNull() {
        actions.forEach(a -> a.setItem(null));
    }
}
