package com.parliament.app.entity.item.action.info;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.parliament.app.entity.item.action.Action;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Entity
@Data
@Table(name = "action_info", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class ActionInfo implements Comparable<ActionInfo> {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    @Nationalized
    private String name;

    @OneToMany(mappedBy = "actionInfo")
    @JsonIgnore
    private List<Action> actions;

    @Override
    public int compareTo(ActionInfo actionInfo) {
        return this.name.compareTo(actionInfo.getName());
    }

    @PreRemove
    private void setActionToNull() {
        actions.forEach(a -> a.setActionInfo(null));
    }
}
