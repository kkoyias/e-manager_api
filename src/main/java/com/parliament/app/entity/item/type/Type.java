package com.parliament.app.entity.item.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.parliament.app.entity.item.Item;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Data
@Entity
@Table(name = "type", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(unique = true)
    @Nationalized
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "type",
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JsonIgnore
    private List<Item> items;

    // set type to null for all items having this particular type on removal
    @PreRemove
    private void setTypeToNull() {
        items.forEach(i -> i.setType(null));
    }
}
