package com.parliament.app.entity.employee;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.parliament.app.entity.building.service.Service;
import com.parliament.app.entity.item.Item;
import com.parliament.app.entity.item.action.Action;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Data
@Entity
@Table(name = "employee", uniqueConstraints = {@UniqueConstraint(columnNames = {"code"})})
public class Employee implements Comparable<Employee> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private long code;

    @Column(name = "first_name")
    @Nationalized
    private String firstName;

    @Column(name = "last_name")
    @Nationalized
    private String lastName;
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "service_id")
    @JsonIgnore
    private Service service;
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Item> items;
    @OneToMany(mappedBy = "actor", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Action> actionsAsActor;
    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Action> actionsAsReceiver;
    @OneToMany(mappedBy = "manager", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Service> myServices;

    private String fullName() {
        return this.firstName + ' ' + this.lastName;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", code=" + code +
                ", fullName='" + firstName + ' ' + lastName + "'" +
                ", service=" + service.getName() +
                ", items=[" + items.stream().map(Item::getId) + ']' +
                '}';
    }

    public String projectedName() {
        return this.code + " " + this.firstName + " " + this.lastName;
    }

    @Override
    public int compareTo(Employee employee) {
        return this.fullName().compareTo(employee.fullName());
    }

    @PreRemove
    private void setEmployeeToNull() {
        items.forEach(i -> i.setOwner(null));
        actionsAsActor.forEach(a -> a.setActor(null));
        actionsAsReceiver.forEach(a -> a.setReceiver(null));
        myServices.forEach(s -> s.setManager(null));
    }
}
