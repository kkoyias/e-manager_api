package com.parliament.app.entity.building;


import com.parliament.app.entity.building.service.Service;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

@Data
@Entity
@Table(name = "building", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Building implements Comparable<Building> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(unique = true)
    @Nationalized
    private String name;

    @Column
    @Nationalized
    private String address;

    @OneToMany(mappedBy = "building", cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private List<Service> services;

    public void addService(Service service) {
        this.services.add(service);
    }

    @PreRemove
    private void setBuildingToNull() {
        services.forEach(s -> s.setBuilding(null));
    }

    @Override
    public int compareTo(Building building) {
        return this.name.compareTo(building.getName());
    }
}

