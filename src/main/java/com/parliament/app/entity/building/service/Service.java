package com.parliament.app.entity.building.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.parliament.app.entity.building.Building;
import com.parliament.app.entity.employee.Employee;
import lombok.Data;
import org.hibernate.annotations.Nationalized;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "service")
public class Service implements Comparable<Service> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(name = "service_name")
    @Nationalized
    private String name;

    @Column(name = "floor")
    private int floor;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JsonIgnore
    @JoinColumn(name = "building_id")
    private Building building;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "parent_id")
    private Service parentService;
    @OneToMany(mappedBy = "parentService", fetch = FetchType.LAZY)
    private List<Service> childServices;
    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Employee manager;
    @OneToMany(mappedBy = "service")
    private List<Employee> employees;

    public String fullName() {
        if (this.parentService == null) {
            return this.name;
        } else {
            return this.name + ", " + this.parentService.fullName();
        }
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", building=" + (building != null ? building.getName() : "-") +
                ", floor=" + floor +
                ", manager= " + (manager != null ? manager.getFirstName() + " " + manager.getLastName() : "-") +
                ", parent-service=" + (parentService != null ? parentService.getName() : "-") +
                ", services=" + childServices.stream().map(Service::getName) +
                ", employees=" + employees.stream().map(Employee::getId) +
                '}';
    }

    @Override
    public int compareTo(Service service) {
        return this.name.compareTo(service.getName());
    }

    @PreRemove
    private void setServiceToNull() {
        childServices.forEach(d -> d.setParentService(null));
    }
}
