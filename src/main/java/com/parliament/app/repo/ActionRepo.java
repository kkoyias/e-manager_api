package com.parliament.app.repo;

import com.parliament.app.controller.response.ActionProjection;
import com.parliament.app.entity.item.action.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(excerptProjection = ActionProjection.class)
public interface ActionRepo extends JpaRepository<Action, Integer>, ActionDAO {
}
