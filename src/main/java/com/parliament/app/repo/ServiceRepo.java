package com.parliament.app.repo;

import com.parliament.app.controller.response.ServiceProjection;
import com.parliament.app.entity.building.service.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(excerptProjection = ServiceProjection.class, path = "services")
public interface ServiceRepo extends Repo, JpaRepository<Service, Integer> {

    @RestResource(path = "parent", rel = "parent")
    Page<Service> findByParentServiceId(@Param("id") Integer id, @Param("page") Pageable page);
}
