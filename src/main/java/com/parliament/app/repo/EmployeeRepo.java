package com.parliament.app.repo;

import com.parliament.app.controller.response.EmployeeProjection;
import com.parliament.app.entity.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(excerptProjection = EmployeeProjection.class, path = "employees")
public interface EmployeeRepo extends JpaRepository<Employee, Integer>, Repo {
}
