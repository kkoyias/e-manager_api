package com.parliament.app.repo;

import com.parliament.app.controller.response.BuildingProjection;
import com.parliament.app.entity.building.Building;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(excerptProjection = BuildingProjection.class)
public interface BuildingRepo extends JpaRepository<Building, Integer> {
}
