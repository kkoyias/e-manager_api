package com.parliament.app.repo;

import com.parliament.app.controller.response.ItemProjection;
import com.parliament.app.entity.item.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(path = "items", excerptProjection = ItemProjection.class)
public interface ItemRepo extends JpaRepository<Item, Integer>, Repo {
}
