package com.parliament.app.repo;

import java.util.Date;

public interface ActionDAO {
    boolean isLatestAction(Date date, Integer itemID);
}
