package com.parliament.app.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;

@CrossOrigin
@RepositoryRestResource
public class ActionDAOImpl implements ActionDAO {

    private final EntityManager entityManager;

    @Autowired
    public ActionDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public boolean isLatestAction(Date date, Integer itemID) {
        String sqlQuery = "select * from action where item_id=:id and action_date > :actionDate";
        Query query = this.entityManager.createNativeQuery(sqlQuery);
        query.setParameter("id", itemID);
        query.setParameter("actionDate", date);
        return query.getResultList().isEmpty();
    }
}
