package com.parliament.app.repo;

import com.parliament.app.controller.response.TypeProjection;
import com.parliament.app.entity.item.type.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(excerptProjection = TypeProjection.class)
public interface TypeRepo extends JpaRepository<Type, Integer> {
}
