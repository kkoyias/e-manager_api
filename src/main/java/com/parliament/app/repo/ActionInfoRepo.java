package com.parliament.app.repo;

import com.parliament.app.controller.response.ActionInfoProjection;
import com.parliament.app.entity.item.action.info.ActionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(
        collectionResourceRel = "actionInfos",
        path = "actionInfos",
        excerptProjection = ActionInfoProjection.class)
public interface ActionInfoRepo extends JpaRepository<ActionInfo, Integer> {
}
