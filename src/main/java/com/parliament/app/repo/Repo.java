package com.parliament.app.repo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Map;

@CrossOrigin
@RepositoryRestResource
public interface Repo {
    Map<?, ?> find(Map<String, Map<String, Object>> payload, Pageable pageable, Class<?> entityProjection)
            throws NoSuchFieldException, IllegalArgumentException;
}
