package com.parliament.app.repo;

import com.parliament.app.entity.building.service.Service;
import com.parliament.app.entity.employee.Employee;
import com.parliament.app.entity.item.Item;
import com.parliament.app.utils.SortUtils;
import com.parliament.app.utils.manager.QueryManager;
import com.parliament.app.utils.manager.ResponseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.data.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class RepoImpl implements Repo {

    private final EntityManager entityManager;
    private final ProjectionFactory projectionFactory;
    private final Map<Class<?>, Map<String, String>> conditionMap;

    @Autowired
    RepoImpl(EntityManager entityManager, ProjectionFactory projectionFactory) {
        this.entityManager = entityManager;
        this.projectionFactory = projectionFactory;

        // define query mappings for each class
        Map<String, String> itemMap = new LinkedHashMap<>();
        itemMap.put("buildingID", "employee_id in " +
                "(select id from employee where service_id in " +
                "(select id from service where building_id=:buildingID))");
        itemMap.put("serviceID", "employee_id in (select id from employee where service_id=:serviceID)");

        Map<String, String> employeeMap = new LinkedHashMap<>();
        employeeMap.put("buildingID", "service_id in (select id from service where building_id=:buildingID)");
        employeeMap.put("managerID", "service_id in (select id from service where manager_id=:managerID)");

        this.conditionMap = new LinkedHashMap<>();
        this.conditionMap.put(Item.class, itemMap);
        this.conditionMap.put(Employee.class, employeeMap);
        this.conditionMap.put(Service.class, null);
    }


    @Override
    public Map<String, Object> find(Map<String, Map<String, Object>> payload,
                                    Pageable pageable, Class<?> entityProjection)
            throws IllegalArgumentException, NoSuchFieldException {

        String simple = "simple", complex = "complex", simplePropsQuery = "", complexPropsQuery = "";
        Class<?> entity = entityProjection.getAnnotation(Projection.class).types()[0];
        String table = entity.getAnnotation(Table.class).name();

        if (payload == null || payload.isEmpty()) {
            throw new IllegalArgumentException("No payload included");
        }

        // create query's header
        String baseOfQuery = "select * from " + table + " where", query = "";

        // extract simple attributes to look for
        if (payload.containsKey(simple) && !payload.get(simple).isEmpty()) {
            simplePropsQuery = QueryManager.createSimpleQueryFromProps(payload.get(simple), entity);
        }

        // extract complex(foreign) attributes to look for
        if (payload.containsKey(complex) && !payload.get(complex).isEmpty()) {
            complexPropsQuery += QueryManager.createComplexQueryFromProps(
                    payload.get(complex),
                    this.conditionMap.get(entity));
        }

        // construct full query string
        query += QueryManager.constructQuery(simplePropsQuery, complexPropsQuery);

        // get info of sorting request getting a tuple of (direction, (fieldName, isColumn))
        Pair<Sort.Direction, Pair<String, Boolean>> isColumnField =
                SortUtils.inspectField(entity, pageable.getSort().toString());

        // sort results by column if requested
        if (isColumnField != null && isColumnField.getSecond().getSecond() && !query.isEmpty()) {
            query += " order by " + isColumnField.getSecond().getFirst() + ' ' + isColumnField.getFirst();
        }

        // if there was at least one field in the payload/query
        if (!query.isEmpty()) {
            // create native query & set parameters
            Query sqlQuery = this.entityManager.createNativeQuery(baseOfQuery + query, entity);
            QueryManager.setQueryParameters(sqlQuery, payload);

            // transform response to a projection, revealing necessary fields only
            Object queryResult = sqlQuery.getResultList().stream()
                    .map(s -> this.projectionFactory.createProjection(entityProjection, s))
                    .collect(toList());

            return ResponseManager.getResponse(new ArrayList<>((Collection<?>) queryResult), pageable);


            // sort list by a complex / class-based property if requested
            //if(isColumnField != null && isColumnField.getValue().getValue() == false)
        }
        return ResponseManager.getResponse(emptyList(), pageable);
    }


}
