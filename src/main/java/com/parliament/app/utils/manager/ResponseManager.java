package com.parliament.app.utils.manager;

import com.parliament.app.utils.SortUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResponseManager {

    @Value("${spring.data.rest.default-page-size}")
    private static final int defaultPageSize = 4;

    public static PageRequest getPageRequest(Integer page, Integer size, String sort) {
        Pair<Sort.Direction, String> defaultOrder = Pair.of(Sort.Direction.ASC, "id");

        // set default parameters if missing
        int actualSize = (size != null) ? size : defaultPageSize;
        int actualPage = (page != null) ? page : 0;
        Pair<Sort.Direction, String> actualSort = sort != null ? SortUtils.getFieldAndOrderBy(sort, ',') : defaultOrder;

        return PageRequest.of(actualPage, actualSize, Sort.by(actualSort.getFirst(), actualSort.getSecond()));
    }

    public static Map<String, Object> getResponse(List<?> result, Pageable pageable) {
        // create a page holder based on the result and the paging & sorting configuration
        PagedListHolder<?> pagedListHolder = ResponseManager.createPageListHolder(result, pageable);

        // set response's meta data
        Map<String, Object> meta = ResponseManager.createMeta(result, pageable);

        // return final response
        return ResponseManager.createResponse(pagedListHolder, meta);
    }

    // create a response with a list as content, also add pagination metadata
    private static Map<String, Object> createResponse(PagedListHolder<?> pagedListHolder, Map<String, Object> meta) {
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("content", pagedListHolder.getPageList());
        response.put("page", meta);

        return response;
    }

    // set meta data for a http response based on the result list & the configuration
    private static Map<String, Object> createMeta(List<?> content, Pageable pageable) {
        Map<String, Object> rv = new LinkedHashMap<>();
        rv.put("size", pageable.getPageSize());
        rv.put("totalElements", content.size());
        rv.put("totalPages", Math.ceil((float) content.size() / pageable.getPageSize()));
        rv.put("number", pageable.getPageNumber());

        return rv;
    }

    // create a page list holder out of a result list and the Sorting & Paging configuration
    private static <E> PagedListHolder<E> createPageListHolder(List<E> content, Pageable pageable) {

        // create the holder and set the rest of attributes
        PagedListHolder<E> pagedListHolder = new PagedListHolder<>(content);
        pagedListHolder.setPageSize(pageable.getPageSize());
        pagedListHolder.setPage(pageable.getPageNumber());
        return pagedListHolder;
    }
}
