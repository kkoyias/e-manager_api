package com.parliament.app.utils.manager;

import com.parliament.app.utils.Utils;
import org.springframework.data.util.Pair;

import javax.persistence.Query;
import java.lang.reflect.Field;
import java.util.Map;

public class QueryManager {

    // create an HQL query for simple properties of some instance
    public static String createSimpleQueryFromProps(
            Map<String, Object> props,
            Class<?> someClass) throws NoSuchFieldException {

        StringBuilder query = new StringBuilder();

        // for each simple field included in the request
        int i = 0;
        for (Map.Entry<String, Object> entry : props.entrySet()) {
            String key = entry.getKey();

            // add a condition to the query
            Field field = someClass.getDeclaredField(key);
            Pair<String, String> pair = Utils.getColumnNameFromField(field);

            query.append(String.format("%s %s", (i++ == 0 ? "" : " and"), createCondition(field, pair)));
        }

        return query.toString();
    }

    public static String createCondition(Field field, Pair<String, String> pair) {

        // use case-insensitive search for strings
        if (field.getType().equals(String.class)) {
            return "lower(" + pair.getFirst() + ") like lower(" + pair.getSecond() + ')';
        } else {
            return pair.getFirst() + '=' + pair.getSecond();
        }
    }

    // create a native sql query for complex attributes
    public static String createComplexQueryFromProps(Map<String, Object> props,
                                                     Map<String, String> conditionMap) throws NoSuchFieldException {
        String query = "";

        // for each simple field included in the request
        int i = 0;
        for (Map.Entry<String, Object> entry : props.entrySet()) {
            String key = entry.getKey();

            // add a condition to the query
            if (conditionMap.containsKey(key)) {
                query += (i++ == 0 ? "" : " and") + ' ' + conditionMap.get(key);
            } else {
                throw new NoSuchFieldException("complex key - " + key);
            }
        }
        return query;
    }

    // set query parameters using case-insensitive search for strings
    public static void setQueryParameters(Query query, Map<String, Map<String, Object>> payload) {

        for (Map.Entry<String, Map<String, Object>> payloadEntry : payload.entrySet()) {
            for (Map.Entry<String, Object> entry : payloadEntry.getValue().entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
    }

    // merge two query conditions into one
    public static String constructQuery(String simplePropsQuery, String complexPropsQuery) {
        String conditions = "";
        if (simplePropsQuery.isEmpty()) {
            conditions += complexPropsQuery;
        } else if (complexPropsQuery.isEmpty()) {
            conditions += simplePropsQuery;
        } else {
            conditions += simplePropsQuery + " and" + complexPropsQuery;
        }
        return conditions;
    }
}
