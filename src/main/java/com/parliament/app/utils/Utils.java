package com.parliament.app.utils;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.util.Pair;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import java.lang.reflect.Field;
import java.util.Set;

public class Utils {

    public static Class<?> findRepo(Set<Class<?>> clazzSet, String path) {
        for (Class<?> repo : clazzSet) {
            if (repo.getAnnotation(RepositoryRestResource.class).path().equals(path)) {
                return repo;
            }
        }
        return null;
    }

    // transform a POJO's field Java name to the corresponding sql Column or JoinColumn name
    public static Pair<String, String> getColumnNameFromField(Field field) {
        Column column = field.getAnnotation(javax.persistence.Column.class);
        JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);

        // distinguish between columns(fields) & joinColumns(references/foreign-keys)
        String key = column != null ?
                // distinguish between named & not named annotated fields
                (column.name().isEmpty() ? field.getName() : column.name())
                :
                (joinColumn.name().isEmpty() ? field.getName() : joinColumn.name());

        return Pair.of(key, ':' + field.getName());
    }
}
