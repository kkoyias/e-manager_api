package com.parliament.app.utils;

import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;

import java.lang.reflect.Field;

public class SortUtils {
    // break a string of type 'field, order' into a pair (field, order)
    public static Pair<Sort.Direction, String> getFieldAndOrderBy(String fieldAndOrder, char sep) {

        // if some kind of ordering was defined, respond appropriately, else use default ordering
        if (fieldAndOrder.indexOf(sep) > 0) {
            String direction = sanitizeString(fieldAndOrder.substring(fieldAndOrder.indexOf(sep) + 1).trim());
            String fieldName = fieldAndOrder.substring(0, fieldAndOrder.indexOf(sep)).trim();
            return Pair.of(Sort.Direction.fromString(direction), fieldName);

        } else {
            return Pair.of(Sort.DEFAULT_DIRECTION, fieldAndOrder.trim());
        }
    }

    // get string until semicolon(;) to deal with malicious inputs
    private static String sanitizeString(String string) {
        final char evilCharacter = ';';
        return string.substring(string.indexOf(evilCharacter) + 1);
    }

    public static Pair<Sort.Direction, Pair<String, Boolean>> inspectField(Class<?> type, String fieldNameOrder) {
        Pair<Sort.Direction, String> fieldAndOrder = getFieldAndOrderBy(fieldNameOrder, ':');

        // if this is an actual field of this class, else return null indicating that this is not an actual field
        try {
            Field field = type.getDeclaredField(fieldAndOrder.getSecond());

            // if it is a column field return it's database name to be appended at the query
            // else set flag to false to indicate that this field is not a column
            if (field.isAnnotationPresent(javax.persistence.Column.class)) {
                return Pair.of(fieldAndOrder.getFirst(),
                        Pair.of(Utils.getColumnNameFromField(field).getFirst(), true));
            } else {
                return Pair.of(fieldAndOrder.getFirst(),
                        Pair.of(field.getName(), false));
            }
        } catch (NoSuchFieldException e) {
            return null;
        }
    }
}
