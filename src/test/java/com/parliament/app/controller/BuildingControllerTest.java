package com.parliament.app.controller;

import com.parliament.app.entity.building.Building;
import com.parliament.app.entity.building.service.Service;
import com.parliament.app.exception.ControllerException;
import com.parliament.app.repo.BuildingRepo;
import com.parliament.app.repo.ServiceRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BuildingControllerTest {
    private static final int BUILDING_ID = 1;
    private static final Service service = new Service();

    @InjectMocks
    BuildingController buildingController;

    @Mock
    BuildingRepo buildingRepo;

    @Mock
    ServiceRepo serviceRepo;

    @Test
    public void testGetFullBuildingList() {
        final List<Building> buildings = Collections.singletonList(new Building());
        when(buildingRepo.findAll()).thenReturn(buildings);

        final ResponseEntity<List<Building>> response = buildingController.getFullBuildingList();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), buildings);
    }

    @Test
    public void testCreateServiceBuildingNotFound() {
        when(buildingRepo.findById(BUILDING_ID)).thenReturn(Optional.empty());
        assertThrows(ControllerException.class, () -> buildingController.createService(service, BUILDING_ID));
    }

    @Test
    public void testCreateServiceBuilding() {
        final Building building = new Building();
        building.setId(BUILDING_ID);
        building.setServices(new ArrayList<>());
        when(buildingRepo.findById(BUILDING_ID)).thenReturn(Optional.of(building));

        buildingController.createService(service, BUILDING_ID);

        assertEquals(service.getBuilding(), building);
        assertEquals(building.getServices(), Collections.singletonList(service));
        verify(serviceRepo).save(service);
    }
}
