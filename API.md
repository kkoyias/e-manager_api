# e-manager_api

This application implements the server side of `e-manager`.
The RESTful api it provides is used by e-manager ui in order
to retrieve & manipulate data from the manager's database.
Details about the api follow

## base-path: `/api`

### Entities & Structure

* Building
  * name
  * address
  * Service
    * name
    * floor
    * ParentService
    * Employee/Manager
      * code
      * first name
      * last name
      * Item
        * barcode
        * Category
        * ActionInfo

### Routes

For each entity the following **end-points** have been implemented

| Method     | Route            | Action                                 | Rest.Controller    | Errors                                                   |
|------------|------------------|----------------------------------------|--------------------|----------------------------------------------------------|
| **POST**   | /{entity}s       | Insert a new entity                    | {Entity}Controller | -                                                        |
| **PUT**    | /{entity}s/id    | Update an existing entity              | {Entity}Controller | **404**: Entity or parent entity not found               |
| **DELETE** | /{entity}s/id    | Delete an existing entity              | {Entity}Controller | **404**: Entity not found, **405**: Entity is referenced |
| **POST**   | /search/{entity}s| Get records that match some criteria   | {Entity}Controller | **400**: Invalid payload, **404**: Property not found    |
