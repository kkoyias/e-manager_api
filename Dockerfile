FROM openjdk:11
ENV SERVER_HOME=/app/server
WORKDIR $SERVER_HOME
COPY build.gradle settings.gradle gradlew $SERVER_HOME/
COPY gradle $SERVER_HOME/gradle
RUN ./gradlew build || return 0
COPY . .
RUN ./gradlew build
CMD ./gradlew bootRun
